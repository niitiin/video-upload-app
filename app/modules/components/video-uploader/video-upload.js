
(function () {

    'use strict';

    angular.module('processStreetApp')
        .component('videoUploader', {
            templateUrl: 'modules/components/video-uploader/video-upload.html',
        })
        .controller('videoUploaderController', videoUploaderController)
        .factory('videoUploaderService', videoUploaderService);

    function videoUploaderController($scope, $sce, videoUploaderService, $ocLazyLoad, $timeout) {
        'ngInject';

        $scope.fileInit = function (file) {
            $timeout(function () {
                console.log(file);
                console.log(JSON.stringify(file));
            }, 100);
        };

        $scope.fileuploaded = function (file) {
            if (file.$state() === 'resolved') {
                $scope.allVideosList =
                    file.$playVideo = function () {
                        $scope.fetchedDetails = true;
                        videoUploaderService.fetchVideoDetail(this.$response().result)
                            .then(function (data) {
                                if (data.status === 'ready') {
                                    file.isVideoReady = true;
                                    $ocLazyLoad.load("//fast.wistia.com/embed/medias/" + data.hashed_id + ".jsonp");
                                    file.playVideoId = $sce.trustAsHtml('<div class="wistia_embed wistia_async_' + data.hashed_id + '" style="height:349px;width:620px"></div>');
                                }
                            });
                    }
                file.$destroy = function () {
                    videoUploaderService.deleteVideo(this.$response().result)
                        .then(function (data) {
                            $scope.clear(file);
                        });
                }
            }
        }
    }

    function videoUploaderService($http, uploadConfig) {
        'ngInject';

        function apiSrvc(method, fileData) {
            return $http({
                method: method,
                url: uploadConfig.apiUrl + '/' + fileData.hashed_id + '.json',
                params: {
                    api_password: uploadConfig.key
                },
                acceptFileTypes: /(\.|\/)(mp4|wav)$/i,
            });
        }

        return {
            fetchVideoDetail: function (fileData) {
                return apiSrvc('GET', fileData).then(function (response) {
                    return response.data;
                }).catch(function (error) {
                    console.log(error);
                    return error;
                });
            },
            deleteVideo: function (fileData) {
                return apiSrvc('DELETE', fileData).then(function (response) {
                    return response.data;
                }).catch(function (error) {
                    console.log(error);
                    return error;
                });
            }
        }
    }
}());