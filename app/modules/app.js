(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name processStreetApp
     * @description This is the process street video-upload application
     * # processStreetApp
     *
     * Main module of the application.
     */
    angular.module('processStreetApp', [
        'ngAnimate',
        'ui.router',
        'oc.lazyLoad',
        'ngMessages',
        'blueimp.fileupload'
    ]);
    /** === Kick start App ===*/
    angular.element(document)
        .ready(function () {
            angular.bootstrap(document, ['processStreetApp'], {
                // strictDi: true
            });
        });
}());