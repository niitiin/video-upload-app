(function () {
    'use strict';

    angular.module('processStreetApp')
        .config(function ($stateProvider, $urlRouterProvider, $locationProvider, fileUploadProvider) {
            'ngInject';
            // Default Route when Page not defined in setup
            $urlRouterProvider.otherwise('/');
            $stateProvider
                .state('/', {
                    url: "/",
                    template: '<div class="container-fluid"><div class="container"><video-uploader></video-uploader></div></div>',
                });
            $locationProvider.hashPrefix('');

            angular.extend(fileUploadProvider.defaults, {
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                url: 'https://upload.wistia.com',
                maxFileSize: 999000,
                acceptFileTypes: /(\.|\/)(mp4|wav)$/i,
                formData: {
                    api_password: '23b4e427743e1334b1934c5773bfa4173f5e97daa03a064d94e7d06f992c17c0'
                }
            });
        })
        .constant('uploadConfig', {
            apiUrl: 'https://api.wistia.com/v1/medias',
            key: '23b4e427743e1334b1934c5773bfa4173f5e97daa03a064d94e7d06f992c17c0'
        });

}());