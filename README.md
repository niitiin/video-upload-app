# Angular Video Uploader

Before Running application

run `yarn install` or `npm install` and `bower install` to install project dependencies.

Run the following gulp commands.

## Build 
Run `gulp` for building

## Development

 Run `gulp serve` for preview.

## Testing

Running `gulp test` will run the unit tests with karma.
