'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var openURL = require('open');
var lazypipe = require('lazypipe');
var rimraf = require('rimraf');
var wiredep = require('wiredep').stream;
var runSequence = require('run-sequence');
// var googlecdn = require('gulp-google-cdn');
var beautify = require('gulp-jsbeautifier');
var TestServer = require('karma').Server;
var browserSync = require('browser-sync').create();

var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'dist',
    test: 'test'
};

var paths = {
    scripts: [appConfig.app + '/**/*.js'],
    styles: [appConfig.app + '/**/*.scss'],
    ignorscripts: [
        appConfig.app + '/**/*.js',
        "!" + appConfig.app + '/modules/app.js',
        "!" + appConfig.app + '/modules/routing/*'
    ],
    test: ['test/spec/**/*.js'],
    testRequire: [
        'test/mock/**/*.js',
        'test/spec/**/*.js'
    ],
    karma: appConfig.test + '/karma.conf.js',
    views: {
        main: appConfig.app + '/index.html',
        files: [appConfig.app + '/**/*.html']
    },
    json: appConfig.app + '/**/*.json'
};

/////////////////////////
// Reusable pipelines //
////////////////////////

var lintScripts = lazypipe()
    .pipe($.jshint, '.jshintrc')
    .pipe($.jshint.reporter, 'jshint-stylish');

var styles = lazypipe()
    .pipe($.sass, {
        outputStyle: 'expanded',
        precision: 10
    })
    .pipe($.autoprefixer, 'last 1 version')
    .pipe(gulp.dest, '.tmp');

gulp.task('styles', function () {
    return gulp.src(paths.styles)
        .pipe(styles())
        .pipe(browserSync.stream());
});

gulp.task('lint:scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(lintScripts());
});

gulp.task('clean:tmp', function (cb) {
    rimraf('./.tmp', cb);
});

gulp.task('start:client', ['browserSync', 'styles'], function () {
    // openURL('http://localhost:9000');
});

gulp.task('browserSync', function () {
    browserSync.init({
        server: {
            baseDir: ["./app", '.tmp'],
            port: 9000,
            routes: {
                '/bower_components': 'bower_components'
            },
            open: true
        }
    });
});

gulp.task('watch', function () {
    $.watch(paths.styles)
        .pipe($.plumber())
        .pipe(styles())
        .pipe(browserSync.stream());

    $.watch(paths.views.files)
        .pipe($.plumber())
        .pipe(browserSync.stream());

    $.watch(paths.scripts)
        .pipe($.plumber())
        .pipe(lintScripts())
        .pipe(browserSync.stream());

    $.watch(paths.test)
        .pipe($.plumber())
        .pipe(lintScripts());

    gulp.watch('bower.json', ['bower']);
});

gulp.task('serve', function (cb) {
    runSequence('clean:tmp', ['lint:scripts'], ['start:client'],
        'watch', cb);
});

gulp.task('serve:prod', function () {
    $.connect.server({
        root: [appConfig.dist],
        livereload: true,
        port: 9000
    });
});

gulp.task('templateCache', function () {
    return gulp.src([appConfig.app + '/**/*.html', '!' + appConfig.app + '/index.html', '!' + appConfig.app + '/404.html'])
        .pipe($.angularTemplatecache({
            module: 'processStreetApp'
        }))
        .pipe(gulp.dest('test/templates/'));
});

gulp.task('test', ['bower:test', 'templateCache'], function (done) {
    new TestServer({
        configFile: __dirname + '/test/karma.conf.js',
    }, done).start();
});


gulp.task('beautify', function () {
    gulp.src([appConfig.app + './*.css', appConfig.app + './*.html', appConfig.app + './*.js'])
        .pipe(beautify())
        // .pipe($.prettify())
        .pipe(gulp.dest(appConfig.app));
});


// inject bower components
gulp.task('bower', function () {
    return gulp.src(paths.views.main)
        .pipe(wiredep({
            directory: 'bower_components',
            ignorePath: '..'
        }))
        .pipe(gulp.dest(appConfig.app));
});

//inject bower components Test
gulp.task('bower:test', function () {
    return gulp.src(paths.karma)
        .pipe(wiredep({
            directory: 'bower_components',
            ignorePath: '../'
        }))
        .pipe(gulp.dest(appConfig.test));
});
///////////
// Build //
///////////

gulp.task('clean:dist', function (cb) {
    rimraf('./dist/**/*', cb);
});


gulp.task('client:build', ['html', 'styles', 'src:js'], function () {
    var jsFilter = $.filter('**/*.js');
    var cssFilter = $.filter('**/*.css');
    var indexHtmlFilter = $.filter(['**/*', '!**/index.html']);

    return gulp.src(paths.views.main)
        .pipe($.useref({
            searchPath: [appConfig.app, '.tmp']
        }))
        .pipe(jsFilter)
        .pipe($.ngAnnotate())
        .pipe($.uglify().on('error', function (e) {
            console.log(e);
            return this.end();
        }))
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe($.minifyCss({
            cache: true
        }))
        .pipe(cssFilter.restore())
        .pipe(indexHtmlFilter)
        .pipe($.rev())
        .pipe(indexHtmlFilter.restore())
        .pipe($.revReplace())
        .pipe(gulp.dest(appConfig.dist));
});


gulp.task('copy:extras', function () {
    return gulp.src(paths.json)
        .pipe(gulp.dest(appConfig.dist));
});
gulp.task('src:js', function () {
    return gulp.src(paths.ignorscripts)
        .pipe($.ngAnnotate())
        .pipe($.uglify())
        .pipe(gulp.dest(appConfig.dist));
});

gulp.task('html', function () {
    return gulp.src([appConfig.app + "/**/*.html", !appConfig.app + "/index.html"], {
        dot: true
    })
        .pipe(gulp.dest(appConfig.dist));
});

gulp.task('images', function () {
    return gulp.src(appConfig.app + '/images/**/*')
        .pipe($.cache($.imagemin({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(appConfig.dist + '/images'));
});

gulp.task('copy:fonts', function () {
    return gulp.src('bower_components/bootstrap/fonts/*')
        .pipe(gulp.dest(appConfig.dist + '/fonts'));
});

gulp.task('build', ['clean:dist'], function () {
    runSequence(['images', 'copy:extras', 'copy:fonts', 'client:build']);
});

gulp.task('default', ['build']);
