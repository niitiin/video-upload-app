'use strict';

describe('Video-uploader', function () {

	var $compile,
		$rootScope,
		$controller, viewElement;

	var $scope, $ctrl;


	// load the controller's module
	beforeEach(module('processStreetApp'));

	// Store references to $controller, $rootScope and $compile
	// so they are available to all tests in this describe block
	beforeEach(inject(function (_$compile_, _$rootScope_, _$controller_) {
		// The injector unwraps the underscores (_) from around the parameter names when matching
		$compile = _$compile_;
		$rootScope = _$rootScope_;
		$controller = _$controller_;

		// Compile a piece of HTML containing the directive
		viewElement = $compile('<video-uploader></video-uploader>')($rootScope);
		// fire all the watches, so the scope will be evaluated
		$rootScope.$digest();

	}));

	describe('Inject video-uploader component to view', function () {

		it('it should render video-uploader', function () {
			// Check that the compiled element contains the templated content
			expect(viewElement.html()).toContain('input');
		});


	});
});
